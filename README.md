# pulseaudio
# Auto Start pulseaudio on kali linux 2016.2
# OS Test:
```txt
Kali Linux, OpenSUSE Leep 42.2, 42.3, 15
```
# Direct Installation:
```bash
 curl -s https://raw.githubusercontent.com/nu11secur1ty/pulseaudio/master/pulseaudio.sh | bash  
```
![image](https://github.com/nu11secur1ty/pulseaudio/blob/master/PulseAudio.png)
[Follow me on:](http://www.nu11secur1ty.com/2016/09/run-pulseaudioautostartkalilinux-20162.html)
